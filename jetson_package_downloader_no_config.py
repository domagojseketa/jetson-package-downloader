from pathlib import Path
from dataclasses import dataclass
from argparse import ArgumentParser

import requests
from bs4 import BeautifulSoup, Tag


PACKAGE_REPO_URL = "https://repo.download.nvidia.com/jetson/"


@dataclass
class PackageInfo:
    """
    A dataclass object with information for update package's file name and download URL.
    Çlass variables:
    file_name -- name of downloaded package
    href      -- download URL for a package
    """
    file_name: str
    href: str


def get_args():
    """
    Function for setting up ArgumentParser and receiving arguments.
    """
    parser = ArgumentParser(description="Jetson Package Downloader")
    parser.add_argument("version", help="JetPack Version")
    parser.add_argument("--xavier", help="Download Xavier Packages", action="store_true")
    parser.add_argument("--orin", help="Download Orin Packages", action="store_true")
    args = parser.parse_args()

    if not (args.xavier or args.orin):
        parser.error('No platform is selected, add --xavier or --orin.')

    if args.xavier and args.orin:
        parser.error("Can't use both platforms, add --xavier or --orin.")

    return args


def main(args):
    """
    Main function.
    """
    platform = get_platform(args.xavier)
    urls = get_package_urls(args, platform)

    download_dir = get_download_locations(args.version)

    print("Downloading packages.")
    download_packages(urls, download_dir)

    print(f"Finished downloading folders into debs_{args.version}. Move debs_{args.version} folder onto your Jetson device.")
    return


def get_package_urls(args, platform):
    """
    Parsers Nvidia's package repository website and returns a list of URLS.
    Arguments:
    args     -- script arguments
    platform -- Jetson platform
    """
    page = requests.get(PACKAGE_REPO_URL)
    soup = BeautifulSoup(page.content, "html.parser")

    urls = {}
    for h2 in soup.find_all("h2"):
        if not h2.a["name"] == f"Jetpack {args.version}":
            continue
        next_node = h2
        while True:
            next_node = next_node.nextSibling
            if isinstance(next_node, Tag):
                if next_node.name == "h2":
                    break
                for h3 in next_node.find_all("h3"):
                    arch = h3.getText()
                    if arch != platform and arch != "common":
                        continue
                    if isinstance(h3.nextSibling, Tag):
                        bq = h3.nextSibling
                        for a in bq.find_all("a"):
                            package_name_and_version = a.getText()
                            package_name = package_name_and_version.split("_")[0]
                            urls[package_name] = PackageInfo(file_name=package_name_and_version, href=a["href"])
    return urls


def get_download_locations(jetpack_version):
    """
    Creates folders for file download and returns their paths.
    Arguments:
    jetpack_version: JetPack version
    """
    script_dir = Path(__file__).parent
    jetpack_version = jetpack_version.replace(" ", "_")
    deb_dir = script_dir / f"debs_{jetpack_version}"
    try:
        deb_dir.mkdir(parents=True, exist_ok=False)
    except FileExistsError:
        print(f"Folder debs_{jetpack_version} already exists. Remove it with: rm -r debs_{jetpack_version}/")
        exit()

    return deb_dir


def download_packages(urls, download_dir):
    """
    Downloads packages.
    Arguments:
    urls         -- list of download URLS
    dependencies -- list of combined JetPack or L4T dependencies
    download_dir -- download folder location
    """
    for package_info in urls.values():
        with requests.get(package_info.href, stream=True) as r:
            r.raise_for_status()
            print(f"Downloading {package_info.file_name}")
            filename = download_dir / package_info.file_name
            with open(filename, "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:
                        f.write(chunk)


def get_platform(is_xavier):
    """
    Get platform code for name of a Jetson device
    Arguments:
    is_xavier -- boolean since only xavier is supported for now.
    """
    return "t194" if is_xavier else "t234"


if __name__ == "__main__":
    ARGS = get_args()
    main(ARGS)
