# Introduction
Nvidia supports updating Jetson devices with Debian packages as one way of updating if target machine doesn't have Internet access. Nvidia doesn't offer pre-made tar or zip package that contains all of the files required for update. All the files need to be downloaded separately. This script removes that hustle and downloads all the update packages for a given device and version of JetPack.
# Instructions
## Prerequisites

 - Python 3.6
## Installing dependencies
```bash
$ pip install -r requirements.txt
```
## Running the script
```bash
$ python jetson_package_downloader_no_config.py <--xavier | --orin> <JetPack Version>
```
Example:
```bash
$ python jetson_package_downloader_no_config.py --xavier 4.6.1
```
Help:
```bash
$ python jetson_package_downloader_no_config.py --help
```
### Supported JetPack Versions
 - 4.6.1

## Limitations
Currently only supported platform is Xavier.
