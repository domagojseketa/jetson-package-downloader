from pathlib import Path
from dataclasses import dataclass
import json
from argparse import ArgumentParser

import requests
from bs4 import BeautifulSoup, Tag


PACKAGE_REPO_URL = "https://repo.download.nvidia.com/jetson/"


@dataclass
class PackageInfo:
    """
    A dataclass object with information for update package's file name and download URL.
    Çlass variables:
    file_name -- name of downloaded package
    href      -- download URL for a package
    """
    file_name: str
    href: str


def load_config(config_path):
    """
    Load a config file for config_path.
    Arguments:
    config_path -- confing file location
    """
    with open(config_path) as f:
        config = json.load(f)
    return config


def get_args():
    """
    Function for setting up ArgumentParser and receiving arguments.
    """
    parser = ArgumentParser(description="Jetson Package Downloader")
    parser.add_argument("version", help="JetPack Version")
    parser.add_argument("--xavier", help="Download Xavier Packages", action="store_true")
    parser.add_argument("--orin", help="Download Orin Packages", action="store_true")
    parser.add_argument("-c", "--config-file", help="Config file", default="config.json")
    args = parser.parse_args()

    if not (args.xavier or args.orin):
        parser.error('No platform is selected, add --xavier or --orin.')

    if args.xavier and args.orin:
        parser.error("Can't use both platforms, add --xavier or --orin.")

    return args


def main(args):
    """
    Main function.
    """
    check_version(args.version)

    platform = get_platform(args.xavier)
    jetpack_dependencies, l4t_dependencies = get_dependencies(args.version, platform)
    urls = get_package_urls(args, platform)

    check_dependencies(urls, jetpack_dependencies, l4t_dependencies)

    jetpack_dir, l4t_dir = get_download_locations()

    print("Downloading JetPack packages.")
    download_packages(urls, jetpack_dependencies, jetpack_dir)

    print("Downloading L4T packages.")
    download_packages(urls, l4t_dependencies, l4t_dir)
    return


def get_package_urls(args, platform):
    """
    Parsers Nvidia's package repository website and returns a list of URLS.
    Arguments:
    args     -- script arguments
    platform -- Jetson platform
    """
    page = requests.get(PACKAGE_REPO_URL)
    soup = BeautifulSoup(page.content, "html.parser")

    urls = {}
    for h2 in soup.find_all("h2"):
        if not h2.a["name"] == f"Jetpack {args.version}":
            continue
        next_node = h2
        while True:
            next_node = next_node.nextSibling
            if isinstance(next_node, Tag):
                if next_node.name == "h2":
                    break
                for h3 in next_node.find_all("h3"):
                    arch = h3.getText()
                    if arch != platform and arch != "common":
                        continue
                    if isinstance(h3.nextSibling, Tag):
                        bq = h3.nextSibling
                        for a in bq.find_all("a"):
                            package_name_and_version = a.getText()
                            package_name = package_name_and_version.split("_")[0]
                            urls[package_name] = PackageInfo(file_name=package_name_and_version, href=a["href"])
    return urls


def check_dependencies(urls, jetpack_dependencies, l4t_dependencies):
    """
    Checks download URLS if some are missing.
    Arguments:
    urls                 -- list of found download URLS
    jetpack_dependencies -- list of JetPack dependencies
    l4t_dependencies     -- list of L4T dependencies
    """
    for dep in jetpack_dependencies:
        if dep not in urls:
            raise KeyError(f"URL for dependency {dep} can't be found. Check if dependency name is on https://repo.download.nvidia.com/jetson/")
    for dep in l4t_dependencies:
        if dep not in urls:
            raise KeyError(f"URL for dependency {dep} can't be found. Check if dependency name is on https://repo.download.nvidia.com/jetson/")


def get_download_locations():
    """
    Creates folders for file download and returns their paths.
    """
    script_dir = Path(__file__).parent

    l4t_dir = script_dir / "l4t"
    l4t_dir.mkdir(parents=True, exist_ok=True)

    jetpack_dir = script_dir / "jetpack"
    jetpack_dir.mkdir(parents=True, exist_ok=True)

    return jetpack_dir, l4t_dir


def download_packages(urls, dependencies, download_dir):
    """
    Downloads packages.
    Arguments:
    urls         -- list of download URLS
    dependencies -- list of combined JetPack or L4T dependencies
    download_dir -- download folder location
    """
    for dep in dependencies:
        package_info = urls[dep]
        with requests.get(package_info.href, stream=True) as r:
            r.raise_for_status()
            print(f"Downloading {package_info.file_name}")

            filename = download_dir / package_info.file_name
            with open(filename, "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:
                        f.write(chunk)


def check_version(version):
    """
    Checks if JetPack version is supported.
    Arguments:
    version -- JetPack version
    """
    if version not in CONFIG["versions"].keys():
        raise ValueError("JetPack version not supported.")


def get_platform(is_xavier):
    """
    Get platform code for name of a Jetson device
    Arguments:
    is_xavier -- boolean since only xavier is supported for now.
    """
    return "t194" if is_xavier else "t210"


def get_dependencies(version, platform):
    """
    Gets dependencies for JetPack version and Jetson platform.
    Arguments:
    version  -- JetPack version
    platform -- Jetson platform
    """
    jetpack_dependencies = CONFIG["versions"][version][platform]["Jetpack"]
    l4t_dependencies = CONFIG["versions"][version][platform]["L4T"]
    return jetpack_dependencies, l4t_dependencies


if __name__ == "__main__":
    ARGS = get_args()
    CONFIG = load_config(ARGS.config_file)
    main(ARGS)
